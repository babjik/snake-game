import { Component, OnInit } from '@angular/core';
import { Direction } from '../direction.enum';
import { LinkedList } from '../linked-list';
import { interval, Subscription } from 'rxjs';
import { LinkedListNode } from '../linked-list-node';



@Component({
  selector: 'app-snake-board',
  templateUrl: './snake-board.component.html',
  styleUrls: ['./snake-board.component.scss']
})
export class SnakeBoardComponent implements OnInit {

  public score = 0;
  public highestScore = 0;
  public bang: boolean = false;

  board_size = 10; 
  pressedKey = null;
  board = [];
  direction: Direction = Direction.DOWN;
  snake: LinkedList = null;
  food: any = null;
  specialFood: boolean = false;
  crash: any = null;

  foodInterval = 1000;
  subscription: Subscription;

  constructor() { 
  }

  ngOnInit() {
    this.initGame();
  }

  initGame() {
    this.highestScore = 0;
    this.bang = false;
    this.crash = null;
    this.score = 0;
    let counter = 1;
    for (let i=0; i<this.board_size; i++) {
      let row = [];
      for(let j=0; j<this.board_size; j++) {
        row[j] = counter++;
      }
      this.board[i] = row;
    }

    this.initSnake();
    this.getNewFood();

    const source = interval(this.foodInterval);
    this.subscription = source.subscribe(val => this.moveSnake());
  }

  keyPressed(event) {
    this.pressedKey = event.keyCode;
    
    if (event.key === 'ArrowUp')
      this.direction = Direction.UP;
    if (event.key === 'ArrowDown')
      this.direction = Direction.DOWN;
    if (event.key === 'ArrowLeft')
      this.direction = Direction.LEFT;
    if (event.key === 'ArrowRight')
      this.direction = Direction.RIGHT;

    this.moveSnake();
  }

  getCellClass(i, j) {

    if (this.bang && this.isCrash(i, j)) {
      return "cell cell-bang";
    }

    if (this.isFoodCell(i, j)) {
      if (this.specialFood) {
        return 'cell cell-special-food'
      }
      return 'cell cell-food';
    }
    
    if (this.isSnakeCell(i, j)) {
      if (this.isSnakeHead(i, j)) {
        return 'cell cell-snake-head';   
      }
      if (this.isSnakeTail(i, j)) {
        return 'cell cell-snake-tail';   
      }
      return 'cell cell-snake-body'; 
    }

    return 'cell';
  }

  moveSnake() {
    if (!this.bang) {
      this.crash = null;
      let coords = this.snake.head.value;
      let newCoords = this.getNextCoordinates(coords, this.direction)
      if (this.isBang(newCoords)) {
        this.crash = newCoords;
        this.gameOver();
        return;
      }
      this.snake.push(newCoords);
      if (this.isFoodConsumed(newCoords)) {
        if (this.specialFood) {
          this.reverse();
        }
        this.increaseScore();
        console.log("Got the food..");
        this.getNewFood();
      } else {
        this.snake.pop();
      }

      console.log("MOVIING IN direction " + this.direction);
    }
  }


  increaseScore() {
    this.score++;
    if (this.highestScore < this.score) {
        this.highestScore = this.score;
    }
  }

  getNextCoordinates(coords, direction: Direction) {
    let row = coords.row;
    let col = coords.col;

    switch(direction) {
      case Direction.UP: {
          row--;
          break;
        }
      case Direction.DOWN: {
          row++;
          break;
        }
      
      case Direction.LEFT: {
        col--;
        break;
      }

      case Direction.RIGHT: {
        col++;
        break;
      }
    }
    return {
      row: row,
      col: col
    };
  }

  isFoodConsumed(coords) {
    return (this.food && coords 
      && this.food.row === coords.row 
      && this.food.col === coords.col);
  }

  resetGame() {
    this.initGame();
    console.log("RESETTING TO INITIAL STATE");
  }


  getNewFood() {
    let skipLoop = false;
    this.specialFood = false;
    while(!skipLoop) {
      let coord = this.getRandomPoit(this.board_size);
      if(!this.isSnakeCell(coord.row, coord.col)) {
        this.food = coord;
        skipLoop = true;
      } 
    }

    const random = Math.floor(Math.random() * 5);
    if (random % 5 === 1)
      this.specialFood = true;

    console.log("GOT NEW FOOD AT:: " + this.food.row + ", " + this.food.col)
  }

  initSnake() {
    const row = Math.floor(this.board.length / 3);
    const col = Math.floor(this.board[0].length / 3);

    this.snake = new LinkedList({
      row: row,
      col: col
    });
  }

  isCrash(row, col) {
    return this.crash 
      && row === this.crash.row 
      && col === this.crash.col
  }

  isFoodCell(row, col) {
    return this.food 
      && row === this.food.row 
      && col === this.food.col
  }

  isSnakeHead(row, col) {
    if (this.snake && this.snake.head) {
      return this.isNodeMatching(row, col, this.snake.head)
    }
    return false;
  }

  isSnakeTail(row, col) {
    if (this.snake && this.snake.head) {
      let node = this.snake.head;
      while (node.next != null) {
        node = node.next;
      }
      return this.isNodeMatching(row, col, node);
    }
    return false;
  }

  isSnakeCell(row, col) {

    if (this.snake && this.snake.head) {
        let node = this.snake.head;
        if (this.isNodeMatching(row, col, node)) 
          return true;
        while(node.next) {
          if (this.isNodeMatching(row, col, node)) 
            return true;
          node = node.next;
        }
    }
    return false;
  }

  isNodeMatching(row, col, node: LinkedListNode) {
    return node && node.value &&  
      row === node.value.row && col === node.value.col;
  }

  getRandomPoit(max) {
    const row = Math.floor(Math.random() * max);
    const col = Math.floor(Math.random() * max);
    
    return {
      row: row,
      col: col
    }
  }

  isBang(coords) {
    // should be in side window
    if (coords.row < 0 || coords.row > this.board_size -1 
      || coords.col < 0 || coords.col > this.board_size -1
      || this.isSnakeCell(coords.row, coords.col)) {
        return true;
      }
    // it should not touch snake tail.
    // TODO: check for snake
    return false;
  }

  gameOver() {
    console.log("Its bang...!");
    this.bang = true;
  }


  reverse() {
    this.snake.reverse();
    switch(this.direction) {
      case Direction.UP:  {
        this.direction = Direction.DOWN;
        break;
      }
      case Direction.DOWN:  {
        this.direction = Direction.UP;
        break;
      }
      case Direction.LEFT:  {
        this.direction = Direction.RIGHT;
        break;
      }
      case Direction.RIGHT:  {
        this.direction = Direction.LEFT;
        break;
      } 
    }
  }

  ngOnDestroy() {
    this.subscription && this.subscription.unsubscribe();
  }


  logSnake() {
    console.log("SHOWING UP SNAKE")
    if (this.snake && this.snake.head) {
      let node = this.snake.head;
      console.log("SNAKE :: [" + node.value.row + " : " + node.value.col + "]");
      while (node.next) {
        console.log("SNAKE :: [" + node.value.row + " : " + node.value.col + "]");
        node = this.snake.head;
      }
    }
  }

}
