import { LinkedListNode } from "./linked-list-node";

export class LinkedList {
    size: number = 0;
    head: LinkedListNode = null;

    constructor(public value: any) {
        let node = new LinkedListNode(value);
        this.head = node;
        this.size++;
    }

    // push element at head
    push(value: any) {
        let node = new LinkedListNode(value);
        node.next = this.head;
        this.head = node;
        this.size++;
    }

    // delete element at end of tail.
    pop() {
        if (this.size > 0) {
            this.size--;
        }
        if (this.head === null) {
            return;
        }

        if (this.head.next === null) {
            this.head = null;
        }

        let secondLast = this.head;
        while (secondLast.next.next != null) {
            secondLast = secondLast.next;
        }

        secondLast.next = null;
    }

    // reverse data

    reverse() {
        let next = null;
        let previous = null;
        let current =  this.head;
        while (current != null) {
            next = current.next;

            current.next = previous;
            previous = current;
            current = next;
        }
        this.head = previous;
    }

}
