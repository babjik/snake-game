export class LinkedListNode {

    public next: LinkedListNode = null;

    constructor(public value: any) {
        this.value = value;
    }
}
